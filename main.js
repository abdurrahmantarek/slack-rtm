const app = new Vue({

	el : '#app',

	data : {

		
		token : "xoxp-398493476615-398677611670-397461802594-9a207abae9e6c9764cec4ba5604f9f89",
		channelName : "",
		channelId : "",
		chatAvailable : false,
		chatMessage : "",
		message : {

			user : "",
			text : "",
			type : "",
		},
		user : "",
		messages : [],
		users : [],

	},

	computed : {

		getName : function () {

			return this.user;
		}
	},

	created () {

		this.usersList();

		axios.get('https://slack.com/api/rtm.connect', {
			    params: {
			      name : this.name,
			      token : this.token,
			      pretty : 1,
			    }
			  })
			  .then( response => {
				
			    let ws = new WebSocket(response.data.url);

			    ws.onmessage = e => {

			    	let output = JSON.parse(e.data);

			    	if (output.type == 'message' && output.user_profile == null && output.channel == this.channelId) {

		    			let message = { text : output.text };

		    			console.log(output);

		    			if(output.user != null) {
		    				
		    				message.type = "other";
		    				message.user = this.getName;

		    			}else {

		    				message.type = "me";
		    				message.user = "Me";

		    			}

		    			this.messages.push(message);	
			    	}
			    }		

			  })
			  .catch(function (error) {
			    console.log(error);
			  });
	},

	methods : {

		/*
		** create channel
		* @param user_ids
		*/
		creatChannel : function () {

			axios.get('https://slack.com/api/channels.create', {
				    params: {
				      name : this.channelName,
				      token : this.token
				    }
				  })
				  .then( response => {

				    this.channelId = response.data.channel.id;
				    this.chatAvailable = true;

				    alert('channel has been created');

				  })
				  .catch( error => {
				    console.log(error);
				  });
		},

		/*
		** Get users in workspace
		* 
		*/
		usersList : function () {

			axios.get('https://slack.com/api/users.list', {
				    params: {
				      token : this.token
				    }
				  })
				  .then( response => {

				    this.users = response.data.members;

				  })
				  .catch( error => {
				    console.log(error);
				  });

		},

		/*
		** Invite user to channel by it ID
		* @param user_ids
		*/
		inviteUser : function (user_id) {

			axios.get('https://slack.com/api/channels.invite', {
				    params: {
				      token : this.token,
				      channel : this.channelId,
				      user : user_id,
				    }
				  })
				  .then( response => {

				  	//get user info 
				  	this.userInfo(user_id);
				    alert('user has been invited');

				  })
				  .catch( error => {
				    console.log(error);
				  });

		},

		/*
		** Send message to channel
		*/
		sendMessage : function () {

			axios.get('https://slack.com/api/chat.postMessage', {
				    params: {
				      channel : this.channelId,
				      text : this.chatMessage,
				      token : this.token
				    }
				  })
				  .then( response => {
				  	this.chatMessage = "";
				  })
				  .catch( error => {
				    console.log(error);
				  });
		},

		/*
		** Get user info by it ID
		* @param user_ids
		*/
		userInfo : function (user_id) {

			axios.get('https://slack.com/api/users.info', {
				    params: {
				      token : this.token,
				      user : user_id
				    }
				  })
				.then(response => {

					this.user = response.data.user.name;
				})
				.catch( error => {
					console.log(error);
				});

		}
	}
});	